import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prototipo_projecto/database/product.dart';

class ProductInformation extends StatefulWidget {

  final Producto producto;
  ProductInformation(this.producto);
  @override
  _ProductInformationState createState() => _ProductInformationState();
}

final productReference = FirebaseDatabase.instance.reference().child('product');


class _ProductInformationState extends State<ProductInformation> {

  List<Producto> items;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Text('Informacion de producto'),
         backgroundColor: Colors.deepPurple[900],
       ),
      body: Stack(
        children: <Widget>[
          wallpaper,
          Container(
            height: 300.0,
            padding: const EdgeInsets.all(20.0),
            child: Card(
              child: Center(
                child: Column(
                  children: <Widget>[

                    Padding(padding: EdgeInsets.only(top: 55)),

                    new Text("Nombre: ${widget.producto.nombre}", style: TextStyle(fontSize: 18)),
                    Padding(padding: EdgeInsets.only(top: 8.0)),
                    Divider(),

                    new Text("Marca: ${widget.producto.marca}", style: TextStyle(fontSize: 18)),
                    Padding(padding: EdgeInsets.only(top: 8.0)),
                    Divider(),

                    new Text("Stock: ${widget.producto.stock}", style: TextStyle(fontSize: 18)),
                    Padding(padding: EdgeInsets.only(top: 8.0)),
                    Divider(),

                  ],
                ),
              ),
            ),
          ),
        ],
      )
    );
  }
  final wallpaper = new Container(
    decoration: new BoxDecoration(
      image: new DecorationImage(image: new AssetImage("assets/wallpaper_1.png"), fit: BoxFit.fill),
    ),
  );
}
