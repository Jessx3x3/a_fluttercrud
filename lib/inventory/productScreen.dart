import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:prototipo_projecto/database/product.dart';

class ProductScreen extends StatefulWidget {

  final Producto producto;
  ProductScreen(this.producto);

  @override
  _ProductScreenState createState() => _ProductScreenState();
}

final productReference = FirebaseDatabase.instance.reference().child('product');

class _ProductScreenState extends State<ProductScreen> {

  List<Producto> items;

  TextEditingController _nombreController;
  TextEditingController _marcaController;
  TextEditingController _stockController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _nombreController = new TextEditingController(text: widget.producto.nombre);
    _marcaController = new TextEditingController(text: widget.producto.marca);
    _stockController = new TextEditingController(text: widget.producto.stock);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Colors.deepPurple[900],
        title: Text('Actualizar producto'),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          wallpaper,
          Container(
            height: 420.0,
            padding: const EdgeInsets.all(20.0),
            child: Card(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[

                    TextField(
                      controller: _nombreController,
                      style: TextStyle(fontSize: 17.0, color: Colors.deepPurple),
                      maxLength: 40,
                      decoration: InputDecoration(
                          icon: Icon(Icons.person_outline),
                          labelText: 'Nombre de producto'
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top:  8.0)),
                    Divider(),

                    TextField(
                      controller: _marcaController,
                      style: TextStyle(fontSize: 17.0, color: Colors.deepPurple),
                      maxLength: 30,
                      decoration: InputDecoration(
                          icon: Icon(Icons.edit),
                          labelText: 'Marca de producto'
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top:  8.0)),
                    Divider(),

                    TextField(
                      controller: _stockController,
                      keyboardType: TextInputType.number,
                      maxLength: 3,
                      style: TextStyle(fontSize: 17.0, color: Colors.deepPurple),
                      decoration: InputDecoration(
                          icon: Icon(Icons.exposure_plus_1),
                          labelText: 'Stock de producto'
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top:  8.0)),
                    Divider(),

                    FlatButton(
                        onPressed: (){
                          if(widget.producto.id != null){
                            productReference.child(widget.producto.id).set({
                              'nombre': _nombreController.text,
                              'marca': _marcaController.text,
                              'stock': _stockController.text,
                            }).then((_){
                              Navigator.pop(context);
                            });
                          }else{
                            productReference.push().set({
                              'nombre': _nombreController.text,
                              'marca': _marcaController.text,
                              'stock': _stockController.text,
                            }).then((_){
                              Navigator.pop(context);
                            });
                          }
                        },
                        child: (widget.producto.id != null) ? Text('Actualizar') : Text('Agregar')
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      )
    );
  }
  final wallpaper = new Container(
    decoration: new BoxDecoration(
      image: new DecorationImage(image: new AssetImage("assets/wallpaper_1.png"), fit: BoxFit.fill),
    ),
  );
}
