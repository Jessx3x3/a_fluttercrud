import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'dart:async';
import 'package:prototipo_projecto/database/product.dart';
import 'package:prototipo_projecto/inventory/productScreen.dart';
import 'package:prototipo_projecto/inventory/product_information.dart';


class ListViewProduct extends StatefulWidget {
  @override
  _ListViewProductState createState() => _ListViewProductState();
}

final productReference = FirebaseDatabase.instance.reference().child('product');

class _ListViewProductState extends State<ListViewProduct> {

  List<Producto> items;
  StreamSubscription<Event> _onProductAddSubscription;
  StreamSubscription<Event> _onProductUpdateSubscription;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    items = new List();
    _onProductAddSubscription = productReference.onChildAdded.listen(_onProductAdd);
    _onProductUpdateSubscription = productReference.onChildAdded.listen(_onProductUpdate);

  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _onProductAddSubscription.cancel();
    _onProductUpdateSubscription.cancel();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
        appBar: AppBar(
          title: Text('Productos'),
          centerTitle: true,
          backgroundColor: Colors.deepPurple[900],
        ),
        body: Stack(
          children: <Widget>[
            wallpaper,
            Center(
              child: ListView.builder(
                itemCount: items.length,
                padding: EdgeInsets.only(top: 20.0),
                itemBuilder: (context, position){
                  return Column(
                    children: <Widget>[
                      Divider(height: 1.0),
                      Container(
                        padding: new EdgeInsets.all(3.0),
                        child: Card(
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: ListTile(
                                      title: Text(
                                        '${items[position].nombre}',
                                        style: TextStyle(
                                          color: Colors.deepPurple[900],
                                          fontSize: 21.0,
                                        ),
                                      ),
                                      subtitle: Text(
                                        '${items[position].marca}',
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 21.0,
                                        ),
                                      ),
                                      onTap: () => _navigateToProductInformation(
                                          context, items[position])),
                                ),
                                IconButton(
                                  icon: Icon(Icons.delete, color: Colors.redAccent),
                                  onPressed: () => _deleteProduct(context, items[position], position),
                                ),
                                IconButton(
                                  icon: Icon(Icons.edit, color: Colors.deepPurple[900]),
                                  onPressed: () => _navigateToProduct(context, items[position]),
                                )
                              ],
                            )
                        ),
                      )
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add, color: Colors.white),
        backgroundColor: Colors.deepPurple[900],
        onPressed: () => _createProduct(context),
      ),
    ));
  }
  final wallpaper = new Container(
    decoration: new BoxDecoration(
      image: new DecorationImage(image: new AssetImage("assets/wallpaper_1.png"), fit: BoxFit.fill),
    ),
  );
  void _onProductAdd(Event event){
    setState(() {
      items.add(new Producto.fromSnapShot(event.snapshot));
    });
  }
  void _onProductUpdate(Event event){

    var oldProductValue = items.singleWhere((product) => product.id == event.snapshot.key);

    setState(() {
      items[items.indexOf(oldProductValue)] = new Producto.fromSnapShot(event.snapshot);
    });
  }
  void _deleteProduct(
      BuildContext context, Producto product, int position) async {
    await productReference.child(product.id).remove().then((_) {
      setState(() {
        items.removeAt(position);
        Navigator.push(context, MaterialPageRoute(builder: (context) => ListViewProduct()));
      });
    });
  }
  void _navigateToProductInformation(BuildContext context, Producto producto) async {
    await Navigator.push(context, MaterialPageRoute(builder: (context) => ProductInformation(producto)),
    );
  }
  void _navigateToProduct(BuildContext context, Producto producto) async {
    await Navigator.push(context, MaterialPageRoute(builder: (context) => ProductScreen(producto)),
    );
  }
  void _createProduct(BuildContext context) async {
    await Navigator.push(context, MaterialPageRoute(builder: (context) => ProductScreen(Producto(null, '','', ''))),
    );
  }
}

