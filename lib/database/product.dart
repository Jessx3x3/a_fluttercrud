import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

class Producto{

  String _id;
  String _nombre;
  String _marca;
  String _stock;

  Producto(this._id, this._nombre, this._marca, this._stock){
  }

  Producto.map(dynamic obj){
    this._nombre = obj['nombre'];
    this._marca = obj['marca'];
    this._stock = obj['stock'];
  }

  String get id => _id;
  String get nombre => _nombre;
  String get marca => _marca;
  String get stock => _stock;

  Producto.fromSnapShot(DataSnapshot snapshot){
    _id = snapshot.key;
    _nombre = snapshot.value['nombre'];
    _marca = snapshot.value['marca'];
    _stock = snapshot.value['stock'];
  }

}